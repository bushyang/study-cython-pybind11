# Study group - Python extension: Cython and pybind11

Created Time: September 15, 2022 5:09 PM

# Agenda

- Cython
- pybind11
- Compare Cython and pybind11
- pybind11 application and advanced techniques (Next week)

# Reference

- [Welcome to Cython’s Documentation — Cython 3.0.0a11 documentation](https://cython.readthedocs.io/en/latest/index.html)
- [pybind11 documentation](https://pybind11.readthedocs.io/en/latest/index.html)

## **TL;DR**

- See conclusion
- Don’t use ctypes

# Cython

## Introduction

- Compile python code to c code
- Provide wrapper to use c/c++ function
- Write c/c++ code in Python

## Cythonize

![Untitled](Study%20group%20-%20Python%20extension%20Cython%20and%20pybind11%203a03df54b46a4920be1054ae402f6255/Untitled.png)

## Why Cython can achieve speed-up

- Avoid overhead of the python interpreter (EX: for-loop)
    - [Example 1-1](https://gitlab.com/bushyang/study-cython-pybind11/-/tree/master/cython/ex1-find-prime)
- Data locality
    - Using C typing can reduce size to fit in CPU cache, while everything in python is an object
    - [Example 1-2](https://gitlab.com/bushyang/study-cython-pybind11/-/tree/master/cython/ex1-find-prime)
- Early binding
    - [Example 1-2](https://gitlab.com/bushyang/study-cython-pybind11/-/tree/master/cython/ex2-rectangle)

## Ways of using Cython

- Native support c library: libc
    
    ```bash
    from cython.cimports.libc import math
    
    def use_libc_math():
        return math.ceil(5.5)
    ```
    
- Augmenting .pxd file
    - [Example 1-3](https://gitlab.com/bushyang/study-cython-pybind11/-/tree/master/cython/ex3-cpp-class)
- Special functions and decorators using Cython module (Fully support after 3.0.0 alpha )
    
    ```bash
    @cython.cclass
    @cython.cfunc
    cython.declare(a=cython.int, b=cython.int)
    ```
    
- Type annotation
    
    ```bash
    x: cython.int
    ```
    

## Calling c++ library

![Untitled](Study%20group%20-%20Python%20extension%20Cython%20and%20pybind11%203a03df54b46a4920be1054ae402f6255/Untitled%201.png)

## Misc

- C++ container
    - [Example 1-4](https://gitlab.com/bushyang/study-cython-pybind11/-/tree/master/cython/ex4-cpp-stl)
- [Embedding Cython modules in C/C++ applications](https://cython.readthedocs.io/en/latest/src/tutorial/embedding.html)

# Pybind11

## Introduction

- Generate python wrapper
- Provide C++ wrapper to call Python C API

![Untitled](Study%20group%20-%20Python%20extension%20Cython%20and%20pybind11%203a03df54b46a4920be1054ae402f6255/Untitled%202.png)

## Calling c++ library

- Calling c++ function
    - [Example 2-1](https://gitlab.com/bushyang/study-cython-pybind11/-/tree/master/pybind11/ex1)
- Wrap C++ class
    - [Example 2-3](https://gitlab.com/bushyang/study-cython-pybind11/-/tree/master/pybind11/ex3) - compared to example 1-3
- Enum
    - [Example 2-4](https://gitlab.com/bushyang/study-cython-pybind11/-/tree/master/pybind11/ex4)

## ****Type conversions****

- Native type in C++, wrapper in Python
    
    ```jsx
    PYBIND11_MODULE(ex3, m) {
        py::class_<Rectangle>(m, "Rect")
            .def(py::init<>());
    ```
    
- Wrapper in C++, native type in Python
    
    ```jsx
    void print_list(py::list my_list) {
        for (auto item : my_list)
            std::cout << item << " ";
    }
    ```
    
    ```jsx
    print_list([1, 2, 3])
    1 2 3
    ```
    
- Converting between native C++ and Python types
    
    ```jsx
    void print_vector(const std::vector<int> &v) {
        for (auto item : v)
            std::cout << item << "\n";
    }
    ```
    
    ```jsx
    print_vector([1, 2, 3])
    1 2 3
    ```
    

## Misc

- Ownership of return object
- [Embedding the interpreter](https://pybind11.readthedocs.io/en/latest/advanced/embedding.html)

# Compare

**Cython**

Pros

- Progressively performance tuning and for learning purposes
- Write wrapper in Python
- Support C++ with using Python style

Cons

- Need to learn a new programming style
- Two layers (.pyd and .py) of wrapping code
- May be confused when is writing python scope and when is writing Cython

**Pybind11**

Pros

- Write wrapper in c++ (No confusion)
- Build system with CMake integration
- Provide fancier wrapper for Python C API

Cons

- Need to familiar with C++
    - Build system
    - C++ 14, 17, 20, 23
- Wrapping c library may have more efforts

## Conclusion

- When to use Cython
    - Who is familiar with Python
    - Progressively performance tuning
- When to use Pybind11
    - Who is familiar with C++
    - Recommended for wrapping existing C/C++ library