from setuptools import setup
from pybind11.setup_helpers import Pybind11Extension

setup(
    ext_modules=[Pybind11Extension(
        "ex3",
        [
            "Rectangle.cpp",
            "Rectangle_wrapper.cpp"
        ],
        include_dirs=["."],
        language="c++",
    )],
)
