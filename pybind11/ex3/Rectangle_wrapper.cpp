#include <pybind11/pybind11.h>
#include "Rectangle.h"

namespace py = pybind11;
using namespace shapes;

PYBIND11_MODULE(ex3, m) {
    py::class_<Rectangle>(m, "Rect")
        .def(py::init<>())
        .def(py::init<int,int,int,int>())
		.def_readwrite("x0", &Rectangle::x0)
		.def_readwrite("x1", &Rectangle::x1)
		.def_readwrite("y0", &Rectangle::y0)
		.def_readwrite("y1", &Rectangle::y1)
        .def("get_area", &Rectangle::getArea)
        //.def("move", &Rectangle::move)
        .def("move", py::overload_cast<int,int>(&Rectangle::move))
        .def("move", py::overload_cast<int>(&Rectangle::move))
        .def("get_size", [](Rectangle &rect){
            int w, h;
            rect.getSize(&w, &h);
            return std::make_tuple(w, h);
        });
}

