#include <pybind11/pybind11.h>

namespace py = pybind11;

int add(int i, int j = 2) {
    return i + j;
}

PYBIND11_MODULE(ex1, m) {
    m.doc() = "pybind11 example plugin"; // optional module docstring
    m.def("add", &add, "A function that adds two numbers", py::arg("i"), py::arg("j") = 2);
    m.attr("ans") = 42;
    py::object test = py::cast("test123");
    m.attr("test") = test;
}