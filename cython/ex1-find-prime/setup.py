from setuptools import setup
from Cython.Build import cythonize

setup(
    ext_modules=cythonize(
        ['find_primes.py'],                   
        annotate=True),                
)