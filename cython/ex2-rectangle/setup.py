from setuptools import setup
from Cython.Build import cythonize

setup(
    ext_modules=cythonize(
        ['rectangle.py'],                   
        language="c++",
        annotate=True),                
)