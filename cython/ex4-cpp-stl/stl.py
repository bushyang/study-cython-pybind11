
import cython
from cython.cimports.libcpp.vector import vector
from cython.cimports.libcpp.string import string

def test_vec():
    vec: vector[cython.int]
    i: cython.int

    for i in range(10):
        vec.push_back(i)

    for x in vec:
        print(x)

def test_string():
    py_bytes_object = b'test123 test123'
    py_unicode_object = u'test567 567'

    cpp_str: string = py_bytes_object
    print(cpp_str)

    cpp_str2: string = py_unicode_object.encode('utf-8')
    print(cpp_str2)

    str_vec: vector[string] = [b'Hello', b'the', b'world']
    print(str_vec)


