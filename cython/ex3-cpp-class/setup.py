from setuptools import Extension, setup
from Cython.Build import cythonize

setup(ext_modules = cythonize(Extension(
        "rect",                                
        sources=["rect.py", "Rectangle.cpp"], 
        language="c++",
), annotate=True))