
import cython
from cython.cimports.Rectangle import Rectangle

@cython.cclass
class PyRectangle:
    c_rect: Rectangle

    def __init__(self, x0: cython.int, y0: cython.int,  x1: cython.int, y1: cython.int):
        self.c_rect = Rectangle(x0, y0, x1, y1)

    def get_area(self):
        return self.c_rect.getArea()

    def get_size(self):
        width: cython.int = 0
        height: cython.int = 0
        self.c_rect.getSize(cython.address(width), cython.address(height))
        return width, height

    def move(self, dx, dy):
        self.c_rect.move(dx, dy)

    @property
    def x0(self):
        return self.c_rect.x0
    @x0.setter
    def x0(self, x0):
        self.c_rect.x0 = x0

    @property
    def x1(self):
        return self.c_rect.x1
    @x1.setter
    def x1(self, x1):
        self.c_rect.x1 = x1

    @property
    def y0(self):
        return self.c_rect.y0
    @y0.setter
    def y0(self, y0):
        self.c_rect.y0 = y0

    @property
    def y1(self):
        return self.c_rect.y1
    @y1.setter
    def y1(self, y1):
        self.c_rect.y1 = y1